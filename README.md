# mocknode

#### 介绍
生成随机数据，模拟接口返回数据

### 安装依赖包命令
npm i

### 运行命令
npm start

### 目录说明
``` bash
|--.vscode #vscode编辑器配置
|--.bin # 项目bin文件
|--.public #项目发布目录
|--.routes #router配置
|--.views #相关视图文件夹  没有使用
|--.gitgnore #git配置文件
|--app.js #程序入口文件
|--package.json #npm 包
```