const fs = require('fs')

function router (app, route) {
  console.log('开始注册路由\n')
  let files = getFiles('../routes/')// fs.readdirSync('./routes')
  let jsFiles = files.filter(f => f.endsWith('.js'))
  jsFiles.forEach((jf) => {
    let actions = require(jf)
    actions.forEach(action => {
      let method = getMethod(action)
      let path = '/api/' + (jf.indexOf('index') > -1 ? '' : jf.substring(10, jf.length - 3))
      console.log(' path:', path, method)
      switch (method) {
        case 'GET':
          app.use(route.get(path, getHandler(action)))
          break
        case 'POST':
          app.use(route.post(path, getHandler(action)))
          break
        default:
          break
      }
    })
  })
  console.log('路由注册完成\n')
}

function getFiles (path) {
  let files = []
  function fn (fpath) {
    let tempFiles = fs.readdirSync(fpath.substring(1))
    tempFiles.forEach(tf => {
      let stat = fs.statSync(fpath.substring(1) + '/' + tf)
      if (stat.isDirectory()) {
        fn(fpath + tf)
      } else {
        files.push(fpath.endsWith('/') ? fpath + tf : fpath + '/' + tf)
      }
    })
  }
  fn(path)
  return files
}

function getMethod (obj) {
  let m = ''
  Object.keys(obj).forEach(key => {
    if (typeof obj[key] === 'string') {
      m = obj[key]
    }
  })

  return m
}

function getHandler (obj) {
  let fn
  Object.keys(obj).forEach(key => {
    if (typeof obj[key] === 'function') {
      fn = obj[key]
    }
  })
  return fn
}

module.exports = router
