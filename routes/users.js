var Random = MOCK.Random
function getUsers (req, res, next) {
  var data = MOCK.mock({
    'list|10': [
      {
        'id|+1': 1,
        'serial_number|1-100': 1,
        'warn_number|1-100': 1,
        'warn_name|1': ['流水线编排服务异常', '磁盘占用超过阈值'],
        'warn_level|1': ['紧急', '重要'],
        warn_detail: '环境IP:10.114.123.12,服务名称:XX',
        create_time: Random.datetime(),
        finish_time: Random.datetime(),
        'contact|4': 'abc'
      }
    ]
  })
  res.send({
    Code: 1,
    Data: data.list
  })
}

function post (req, res, next) {
  console.log('req', req.body)
  var data = 'aaa'
  res.send({
    Code: 1,
    Data: data
  })
}

module.exports = [
  {
    GET: 'GET',
    getUsers
  },
  {
    POST: 'POST',
    post
  }
]
