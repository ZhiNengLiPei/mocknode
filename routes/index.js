/* GET home page. */
function index (req, res, next) {
  res.render('index', { title: 'Express' })
}

let handler = [{
  'GET': 'GET',
  index
}]

module.exports = handler
